using System;
using System.Collections.Generic;


namespace AD
{
    public partial class MergeSort : Sorter
    {
        protected InsertionSort InsertionSort;
        public MergeSort()
        {
            InsertionSort = new InsertionSort();
        }

        public override void Sort(List<int> list)
        {
            if (list.Count <= 1)
                return;
            IList<int> sorted = InternalSort(list);
            list.Clear();
            list.AddRange(sorted);
        }

        protected IList<int> InternalSort(IList<int> list)
        {
            // base case: a list with 1 or 0 values has nothing to sort with anymore
            if (list.Count <= 1)
                return list;

            // split into left and right list
            IList<int> left = new List<int>();
            IList<int> right = new List<int>();
            int centre = list.Count / 2;
            for (int i=0; i<list.Count; ++i)
            {
                if (i < centre)
                    left.Add(list[i]);
                else
                    right.Add(list[i]);
            }

            // sort left and right list
            left = InternalSort(left);
            right = InternalSort(right);
            // merge em back together
            return Merge(left, right);
        }

        /// <summary>
        /// Merges two lists together, sorted
        /// </summary>
        /// <param name="listA">a sorted sublist</param>
        /// <param name="listB">another sorted sublist</param>
        /// <returns>sorted combo of listA and listB</returns>
        protected IList<int> Merge(IList<int> listA, IList<int> listB)
        {
            IList<int> mergedList = new List<int>();
            int left = 0;
            int right = 0;

            while(left < listA.Count || right < listB.Count)
            {
                // if left is smaller put that in the work array
                if (
                    right >= listB.Count // if right side came to the last number, we just have to add left side
                    || left < listA.Count && listA[left] < listB[right])
                {
                    mergedList.Add(listA[left]);
                    left++;
                }
                else
                {
                    mergedList.Add(listB[right]);
                    right++;
                }
            }

            return mergedList;
        }

        ///// <summary>
        ///// merges/sorts fromArray into intoArray
        ///// splits fromArray into two halves using the begin, centre and end params.
        ///// 
        ///// Left array = begin..centre-1
        ///// Right array = centre..end
        ///// </summary>
        ///// <param name="list"></param>
        ///// <param name="begin">fromArray index where left list starts</param>
        ///// <param name="centre">fromArray index where right list starts</param>
        ///// <param name="end">fromArray index where right list ends</param>
        ///// <returns></returns>
        //protected IList<int> Merge(List<int> list, int begin, int centre, int end)
        //{
        //    IList<int> mergedList = new List<int>();
        //    int leftIndex = begin;
        //    int rightIndex = centre;

        //    // for each number because we know:
        //    // the amount of operations = the amount of numbers.
        //    for (int i = begin; i < end; ++i)
        //    {
        //        // if left is bigger put that in the work array
        //        if (leftIndex != centre && list[leftIndex] < list[rightIndex])
        //        {
        //            mergedList[i] = list[leftIndex];
        //            leftIndex++;
        //        }
        //        else
        //        {
        //            mergedList[i] = list[rightIndex];
        //            rightIndex++;
        //        }
        //    }

        //    return mergedList;
        //}

    }
}
