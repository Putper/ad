using System;
using System.Collections.Generic;
using System.Reflection;


namespace AD
{
    public partial class QuickSort : Sorter
    {
        /// <summary>
        /// Its not worth doing quicksort for tiny arrays.
        /// A good cutoff is 10 elements, although any cutoff between 5 and 20 is likely to produce similar results.
        /// </summary>
        private static int CUTOFF = 10;
        protected Sorter InsertionSort;

        public QuickSort()
        {
            InsertionSort = new InsertionSort();
        }

        public override void Sort(List<int> list)
        {
            if (list.Count <= 1)
                return;
            Sort(list, 0, list.Count - 1);
        }

        protected void Sort(List<int> list, int low, int high)
        {
            // its not worth doing quicksort for tiny arrays. We'll use insertion sort instead
            if (low + CUTOFF > high)
            {
                InsertionSort.Sort(list);
                return;
            }

            // sort low, middle & high
            int middle = (low + high) / 2;
            if (list[middle] < list[low])
                SwapReferences(list, low, middle);
            if (list[high] < list[low])
                SwapReferences(list, low, high);
            if (list[high] < list[middle])
                SwapReferences(list, middle, high);

            // place pivot at position high -1
            SwapReferences(list, middle, high - 1);
            int pivot = list[high - 1];

            // partition
            int i = low;
            int j = high - 1;
            while (true)
            {
                while (list[++i] < pivot) ;
                while (pivot < list[--j]) ;
                if (i >= j)
                    break;
                SwapReferences(list, i, j);
            }
            // restore pivot
            SwapReferences(list, i, high - 1);

            Sort(list, low, i - 1); // sort left side (elements SMALLER than pivot)
            Sort(list, i + 1, high); // sort right side (elements LARGER than pivot
        }

        /// <summary>
        /// Swaps items in a list.
        /// SwapReferences([0,1,2,3,4], 1, 3) changes list to [0,3,2,1,4]
        /// </summary>
        /// <param name="list"></param>
        /// <param name="indexA"></param>
        /// <param name="indexB"></param>
        protected void SwapReferences(IList<int> list, int indexA, int indexB)
        {
            (list[indexB], list[indexA]) = (list[indexA], list[indexB]);
            /*int temp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = temp;*/
        }
    }
}
