using System.Collections.Generic;


namespace AD
{
    public partial class ShellSort : Sorter
    {
        public override void Sort(List<int> list)
        {
            if (list.Count <= 1)
                return;

            SortByIncrement(list, 5);
            SortByIncrement(list, 3);
            SortByIncrement(list, 1);
            //(new InsertionSort()).Sort(list);
        }

        private void SortByIncrement(List<int> list, int increment)
        {
            for(int i = increment; i<list.Count; ++i)
            {
                // store the original list[i] value and leave it empty for shifting
                int originalI = list[i];
                // shift earlier sorted elements up until the correct location for list[i] is found
                int j = 0;
                for(j=i; j >= increment && list[j-increment] > originalI; j-=increment)
                {
                    list[j] = list[j - increment];
                }
                // put the original list[i] value in the correct place
                list[j] = originalI;
            }
        }
    }
}
