using System.Collections.Generic;


namespace AD
{
    public partial class InsertionSort : Sorter
    {
        public override void Sort(List<int> list)
        {
            Sort(list, 0, list.Count - 1);
        }

        public void Sort(List<int> list, int lo, int hi)
        {
            // base case: sort until reach last number
            if (lo <= hi)
            {
                // first element is always already sorted
                if (lo != 0)
                {
                    // grab the current lo position
                    // compare it to its previous number, if that's bigger swap them around
                    // do that until we find the right position or reach beginning of array
                    for (int i = lo; i > 0 && list[i - 1] > list[i]; --i)
                    {
                        int current = list[i]; // temporary store so we dont lose
                        list[i] = list[i - 1];
                        list[i - 1] = current;
                    }
                }

                // sort next
                Sort(list, lo + 1, hi);
            }
        }
    }
}
