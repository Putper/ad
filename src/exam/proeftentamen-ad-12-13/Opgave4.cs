﻿using System;
using System.Collections.Generic;
using System.Text;
using AD;

namespace ad_12_13
{
    internal class Opgave4
    {
        /// <summary>
        /// 4a
        /// </summary>
        /// <returns></returns>
        public Graph CreateGraph()
        {
            Graph graph = new Graph();
            graph.AddEdge("A", "B");
            graph.AddEdge("A", "G");
            graph.AddEdge("C", "B");
            graph.AddEdge("D", "F");
            graph.AddEdge("D", "G");
            graph.AddEdge("E", "C");
            graph.AddEdge("F", "F");
            graph.AddEdge("G", "D");
            graph.AddEdge("G", "F");
            graph.AddEdge("G", "H");
            graph.AddEdge("H", "E");
            graph.AddEdge("H", "K");
            graph.AddEdge("K", "G");
            return graph;
        }

        /// <summary>
        /// 4b
        /// calculate shortest distance to edge "G"
        /// </summary>
        /// <param name="graph"></param>
        public void VulAfstand(Graph graph)
        {
            graph.Unweighted("G");
            foreach (KeyValuePair<string, Vertex> pair in graph.vertexMap)
            {
                Console.WriteLine($"{pair.Key}: {pair.Value.distance}");
            }
            return;
            // TODO: calculate G and use that instead
            string target = "G";
            Dictionary<string, double> distances = new Dictionary<string, double>();

            // foreach vertex, calculate distance to target
            foreach (string vertex in graph.vertexMap.Keys)
            {
                graph.ClearAll();
                double distance;
                if(vertex != target)
                {
                    graph.Unweighted(vertex);
                    distance = graph.GetVertex(target).distance;
                }
                // distance to self is 0
                else
                    distance = 0;

                if (distance == Graph.INFINITY)
                    distance = -1;

                distances.Add(vertex, distance);
            }

            // mark in graph
            foreach (KeyValuePair<string, Vertex> pair in graph.vertexMap)
                pair.Value.distance = distances[pair.Key];
        }

        /// <summary>
        /// 4b
        /// </summary>
        /// <param name="graph"></param>
        public void ToonAfstand(Graph graph)
        {
            Console.WriteLine("Distance for each Edge");
            foreach (KeyValuePair<string, Vertex> pair in graph.vertexMap)
            {
                Console.WriteLine($"{pair.Key}: {pair.Value.GetDistance()}");
            }
        }

        /// <summary>
        /// 4c
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="vertexName"></param>
        /// <returns></returns>
        public bool HasCycle(Graph graph, string vertexName)
        {
            graph.ClearAll();
            Vertex target = graph.GetVertex(vertexName);
            target.known = true;

            Queue<Vertex> queue = new Queue<Vertex>();
            queue.Enqueue(target);

            // explore first in queue, and add all adjacents
            // to the queue to explore them as well
            while (queue.Count > 0)
            {
                Vertex vertex = queue.Dequeue();
                foreach (Edge edge in vertex.GetAdjacents())
                {
                    Vertex neighbour = edge.dest;
                    // Found self!
                    if (neighbour.Equals(target))
                        return true;
                    // traverse neighbours if not already done before
                    if(!neighbour.known)
                    {
                        neighbour.known = true;
                        queue.Enqueue(neighbour);
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 4d
        /// </summary>
        /// <param name="graph"></param>
        public void ToonCycles(Graph graph)
        {
            Console.WriteLine("Cycles for graph:");
            foreach(string vertex in graph.vertexMap.Keys)
            {
                bool result = HasCycle(graph, vertex);
                Console.WriteLine($"{vertex}: {result}");
            }
        }
    }
}
