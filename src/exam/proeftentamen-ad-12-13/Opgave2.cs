﻿using System;
using System.Collections.Generic;
using System.Text;
using AD;

namespace ad_12_13
{
    internal class Opgave2
    {
        public int GeefEenNaKleinsteElement(IBinarySearchTree<int> tree)
        {
            if (tree.IsEmpty()) // need at least 2 numbers to get second smallest element
                throw new BinarySearchTreeEmptyException();

            BinaryNode<int> lastNode = tree.GetRoot();
            BinaryNode<int> currentNode = lastNode.GetLeft();
            
            if (currentNode == null) // need at least 2 numbers to get second smallest element
                throw new BinarySearchTreeTooSmallException();

            // keep shifting smaller numbers into secondSmallest and smallest vars until no smaller left
            while (currentNode.GetLeft() != null)
            {
                lastNode = currentNode;
                currentNode = currentNode.GetLeft();
            }

            // situation A: There's no nodes on the right side
            // that means the second to smallest is the node above the currentNode (aka lastNode)
            if (currentNode.GetRight() == null)
                return lastNode.GetData();

            // Situation B: There's nodes left on the right side
            // this means the second to smallest elemenet
            // is the smallest one on the right side
            currentNode = currentNode.GetRight();
            while (currentNode.GetLeft() != null)
            {
                currentNode = currentNode.GetLeft();
            }
            return currentNode.GetData();
        }

        /// <summary>
        ///     6
        ///    / \
        ///   2   8
        ///  / \
        /// 1   4
        ///    /
        ///   3
        /// </summary>
        /// <returns>a small BinarySearchTree with 6 elements</returns>
        public BinarySearchTree<int> CreateTree(bool exclude1=false)
        {
            BinarySearchTree<int> tree = new BinarySearchTree<int>();
            BinaryNode<int> t3 = new BinaryNode<int>(3, null, null);
            BinaryNode<int> t1 = null;
            if (!exclude1)
                t1 = new BinaryNode<int>(1, null, null);
            BinaryNode<int> t4 = new BinaryNode<int>(4, t3, null);
            BinaryNode<int> t2 = new BinaryNode<int>(2, t1, t4);
            BinaryNode<int> t8 = new BinaryNode<int>(8, null, null);
            BinaryNode<int> t6 = new BinaryNode<int>(6, t2, t8);
            tree.root = t6;
            return tree;
        }
    }
}


public class BinarySearchTreeTooSmallException : System.Exception
{
}