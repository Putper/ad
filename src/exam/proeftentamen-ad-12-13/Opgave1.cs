﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ad_12_13
{
    internal class Opgave1
    {
        public string PrintLetters(int n)
        {
            if (n <= 0)
                return string.Empty;
            return $"A{PrintLetters(n-1)}Z";
        }

        //public void PrintLetters(int n)
        //{
        //    Console.WriteLine(GetLetters('A', n) + GetLetters('Z', n));
        //}

        //private string GetLetters(char letter, int count)
        //{
        //    if (count == 1)
        //        return letter.ToString();
        //    return letter + GetLetters(letter, count - 1);
        //}

        public string PrintLetters2(int p, int q)
        {
            string result = string.Empty;
            if (p > 0)
                result += "A";
            if(p > 0 || q > 0)
                result += PrintLetters2(p - 1, q - 1);
            if (q > 0)
                result += "Z";
            return result;
        }
    }
}
