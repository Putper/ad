﻿using System;
using System.Collections.Generic;
using System.Text;
using AD;

namespace ad_12_13
{
    internal class Opgave3
    {
        public static readonly int DEFAULT_VALUE = -1;

        /// <summary>
        /// 3a
        /// </summary>
        /// <param name="tree"></param>
        /// <returns></returns>
        public bool IsComplete(int[] tree)
        {
            bool encounteredDefault = false;
            for(int i=1; i<tree.Length; ++i)
            {
                // if we've encountered a defaultValue
                if (tree[i] == DEFAULT_VALUE)
                    encounteredDefault = true;
                // we shouldn't encounter any real values anymore
                else if (encounteredDefault)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 3b
        /// </summary>
        /// <param name="tree"></param>
        /// <returns></returns>
        public bool IsMaxHeap(int[] tree)
        {
            if (!IsComplete(tree))
                return false;
            for(int i=1; i<tree.Length; ++i)
            {
                // not a filled value
                if (tree[i] == DEFAULT_VALUE)
                    continue;
                // return false if a child is bigger than current
                int leftIndex = i * 2;
                int rightIndex = leftIndex + 1;
                if (leftIndex < tree.Length && tree[leftIndex] != DEFAULT_VALUE && tree[leftIndex] > tree[i]
                    || rightIndex < tree.Length && tree[rightIndex] != DEFAULT_VALUE && tree[rightIndex] > tree[i])
                    return false;
            }
            return true;
        }

        /// <summary>
        ///     10
        ///    /  \
        ///   4    7
        ///  / \    \
        /// 1   3    5
        /// </summary>
        /// <returns></returns>
        public int[] CreateTree1()
        {
            return new int[] { DEFAULT_VALUE, 10, 4, 7, 1, 3, DEFAULT_VALUE, 5 };
            //PriorityQueue<int> tree = new PriorityQueue<int>();
            //treeArray[1] = 10;
            //treeArray[2] = 4;
            //treeArray[3] = 7;
            //treeArray[4] = 1;
            //treeArray[5] = 3;
            //treeArray[7] = 5;
            //return tree;
        }

        /// <summary>
        ///        15
        ///      /    \
        ///     5      11
        ///    / \    /  \
        ///   3   4  10   7
        ///  /
        /// 1
        /// </summary>
        /// <returns></returns>
        public int[] CreateTree2()
        {
            return new int[] { DEFAULT_VALUE, 15, 5, 11, 3, 4, 10, 7, 1 };
            //PriorityQueue<int> tree = new PriorityQueue<int>();
            //tree.array[1] = 15;
            //tree.array[2] = 5;
            //tree.array[3] = 11;
            //tree.array[4] = 3;
            //tree.array[5] = 4;
            //tree.array[6] = 10;
            //tree.array[7] = 7;
            //tree.array[8] = 1;
            //return tree;
        }


    }
}
