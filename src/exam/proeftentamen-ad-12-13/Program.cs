﻿using System;
using AD;

namespace ad_12_13
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine();

            Opgave2();

            Console.WriteLine();
            Console.WriteLine();

            Opgave3();

            Console.WriteLine();
            Console.WriteLine();

            Opgave4();

            Console.ReadLine();
        }

        static void Opgave1()
        {
            Opgave1 opgave1 = new Opgave1();
            Console.WriteLine("OPGAVE 1");
            Console.WriteLine("________");
            Console.WriteLine("->PrintLetters(4)");
            Console.WriteLine(opgave1.PrintLetters(4));
            Console.WriteLine("->PrintLetters2(3,5)");
            Console.WriteLine(opgave1.PrintLetters2(3, 5));
        }

        static void Opgave2()
        {
            Opgave2 opgave2 = new Opgave2();
            Console.WriteLine("OPGAVE 2");
            Console.WriteLine("________");
            Console.WriteLine("tree:");
            Console.WriteLine(
         "    6\n" +
         "   / \\\n" +
         "  2   8\n" +
         " / \\\n" +
         "1   4\n" +
         "   /\n" +
         "  3");
            Console.WriteLine("->GeefEenNaKleinsteElement(tree)");
            Console.WriteLine(opgave2.GeefEenNaKleinsteElement(opgave2.CreateTree()));

            Console.WriteLine();
            Console.WriteLine("treeWithout1:");
            Console.WriteLine(
         "    6\n" +
         "   / \\\n" +
         "  2   8\n" +
         "   \\\n" +
         "    4\n" +
         "   /\n" +
         "  3");
            Console.WriteLine("->GeefEenNaKleinsteElement(treeWithout1)");
            Console.WriteLine(opgave2.GeefEenNaKleinsteElement(opgave2.CreateTree(true)));
        }

        static void Opgave3()
        {
            Opgave3 opgave3 = new Opgave3();
            Console.WriteLine("OPGAVE 3");
            Console.WriteLine("________");
            int[] tree1 = opgave3.CreateTree1();
            Console.WriteLine("tree1:");
            Console.WriteLine(
            "    10\n" +
            "   /  \\\n" +
            "  4    7\n" +
            " / \\    \\\n" +
            "1   3    5\n"
            );
            Console.WriteLine("->IsComplete(tree1)");
            Console.WriteLine(opgave3.IsComplete(tree1));
            Console.WriteLine("->IsMaxHeap(tree1)");
            Console.WriteLine(opgave3.IsMaxHeap(tree1));

            int[] tree2 = opgave3.CreateTree2();
            Console.WriteLine("tree2:");
            Console.WriteLine(
            "       15\n" +
            "     /    \\\n" +
            "    5      11\n" +
            "   / \\    /  \\\n" +
            "  3   4  10   7\n" +
            " /\n" +
            "1\n"
            );
            Console.WriteLine("->IsComplete(tree2)");
            Console.WriteLine(opgave3.IsComplete(tree2));
            Console.WriteLine("->IsMaxHeap(tree2)");
            Console.WriteLine(opgave3.IsMaxHeap(tree2));
        }

        static void Opgave4()
        {
            Opgave4 opgave4 = new Opgave4();
            Console.WriteLine("OPGAVE 4");
            Console.WriteLine("________");
            Graph graph = opgave4.CreateGraph();
            opgave4.VulAfstand(graph);
            Console.WriteLine("->ToonAfstand()");
            opgave4.ToonAfstand(graph);

            Console.WriteLine();
            Console.WriteLine("->ToonCycles()");
            opgave4.ToonCycles(graph);
        }
    }
}
