﻿using System.Collections.Generic;

namespace AD
{
    public partial class Graph
    {
        public Vertex HoogsteGraad()
        {
            return HoogsteGraad(vertexMap.Values);
        }
        private Vertex HoogsteGraad(IEnumerable<Vertex> vertices)
        {
            int highestValue = 0;
            Vertex highestVertex = null;
            foreach (Vertex vertex in vertices)
            {
                int graad = vertex.Graad();
                if (graad > highestValue)
                {
                    highestValue = graad;
                    highestVertex = vertex;
                }
            }
            return highestVertex;
        }

        public HashSet<Vertex> MaximaleKliek()
        {
            // begin met een lege set van vertices. dit wordt de kliek
            HashSet<Vertex> kliek = new HashSet<Vertex>();
            // voeg nu de vertex met de hoogste graad teo aan de kliek
            kliek.Add(HoogsteGraad());
            // herhaal het volgende:
            return MaximaleKliek(kliek);
        }

        public HashSet<Vertex> MaximaleKliek(HashSet<Vertex> kliek)
        {
            // bekijk alle vertices die verbonden zijn met alle vertices die in de kliek zitten
            HashSet<Vertex> connected = new HashSet<Vertex>();
            foreach(Vertex vertex in vertexMap.Values)
            {
                if(vertex.IsVerbondenMetAllemaal(kliek))
                    connected.Add(vertex);
            }
            // Is er gene vertex meer die verbonden ism et alle vertices in de kliek
            // dan ben je klaar en heb je een maximale kliek gevonden
            // (base case)
            if (connected.Count == 0)
                return kliek;

            // van al deze verticse, voeg degene met de hoogste graad toe aan de kliek
            kliek.Add(HoogsteGraad(connected));

            // Herhaal het volgende
            return MaximaleKliek(kliek);
        }
    }
}
