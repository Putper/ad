﻿using System.Collections.Generic;
using System.Linq;

namespace AD
{
    public partial class Vertex
    {
        /// <summary>
        /// assumes an Undirected graph as presented in the exam
        /// </summary>
        /// <returns>the amount of adjencies this vertex has</returns>
        public int Graad()
        {
            return adj.Count;
        }

        public bool IsVerbondenMetAllemaal(HashSet<Vertex> vertices)
        {
            foreach(Vertex vertex in vertices)
            {
                var result = adj.FirstOrDefault(edge => edge.dest.Equals(vertex));
                if (result == null)
                    return false;
            }
            return true;
        }
    }
}
