﻿using System;
using System.Text.RegularExpressions;


namespace AD
{
    public class BigInt
    {
        public BigIntDigit first;
        public bool isPositive = true;

        public BigInt(string number)
        {
            // null/0 gegeven? 1 node aanmaken met waarde 0
            // hier heb ik " || number.Equals("0")" aan toegevoegd!!
            if (number == null || number.Length == 0 || number.Equals("0"))
            {
                first = new BigIntDigit(0);
                isPositive = true;
                return;
            }

            // voldoet niet aan definitie van een getal
            Regex valid_Regex = new Regex(@"^(0|(-?[1-9][0-9]*))$", 0);
            if (!valid_Regex.IsMatch(number))
            {
                throw new BigIntWrongInputException();
            }

            /*
             * From here you can start to build your list of BigIntDigit's.
             */
            BigIntDigit previous = null;
            // enkele for loop = O(N)
            for(int i=0; i<number.Length; ++i)
            {
                // a minus sign means the number is negative
                if(number[i].Equals('-'))
                {
                    isPositive = false;
                    continue;
                }

                // should always be a number because of regex and within int range because its between 0-9
                int value = (int)Char.GetNumericValue(number, i); 

                BigIntDigit digit = new BigIntDigit(value);
                if (previous != null) // chain
                    digit.next = previous;

                previous = digit;
            }
            first = previous;
        }

        /// <summary>
        /// O(N) want hij gaat 1x recursief door alle first.next.next
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // add minus if negative
            string prefix = (isPositive)
                ? ""
                : "-";
            return prefix + ToString(first);
        }
        /// <summary>
        /// give a digit, get it's value and the value's of it's children back
        /// ex 5->4->3 will return 345
        /// </summary>
        /// <param name="digit">digit + children's values</param>
        /// <returns></returns>
        private string ToString(BigIntDigit digit)
        {
            if (digit == null)
                return String.Empty;
            return ToString(digit.next) + digit.value;
        }

        /// <summary>
        /// de vorige deden we recursief doen we deze lekker anders :)
        /// O(N) want hij doet een while() loop door alle first.next.next
        /// </summary>
        /// <returns>3->2->1 = 6 want 3+2+1=6</returns>
        public int Sum()
        {
            int sum = 0;
            BigIntDigit currentDigit = first;
            while(currentDigit != null)
            {
                sum += currentDigit.value;
                currentDigit = currentDigit.next;
            }
            return sum;
        }

        public void Increment()
        {
            IncrementPositive(first);
        }
        /// <summary>
        /// O(N) want hij gaat recursief 1 keer langs de digits via first.next.next tot hij is incremented
        /// </summary>
        /// <param name="digit"></param>
        private void IncrementPositive(BigIntDigit digit)
        {
            // with 9 its harder to increment. incrementing 129 makes it 130
            // incrementing 1299999 makes it 130000
            if (digit.value == 9)
            {
                digit.value = 0;
                if(digit.next == null)
                {
                    // if there's no next digit to increment, a new one needs to be introduced
                    // for example incrementing 9 makes it 10
                    digit.next = new BigIntDigit(1);
                }
                else
                    IncrementPositive(digit.next);
            }
            // if its lower than 9 we can increment it directly
            else
                digit.value++;
        }

        /// <summary>
        /// O(N) want hij gaat 1x door de digits heen tot hij is geincrement
        /// via IncrementPositive() of via IncrementNegative()
        /// </summary>
        public void IncrementBetter()
        {
            if (isPositive)
                Increment();
            else
            {
                // -1 becomes 0 (positive)
                if(first.value == 1 && first.next == null)
                {
                    isPositive = true;
                    first.value = 0;
                }
                else
                    IncrementNegative(first);
            }
        }
        private void IncrementNegative(BigIntDigit digit)
        {
            if (digit.value == 0)
            {
                digit.value = 9;
                // if the first value in the number is a 1 it needs to be deleted
                // example -1000 becomes -999
                if (digit.next != null && digit.next.value == 1 && digit.next.next == null)
                    digit.next = null;
                else
                    IncrementNegative(digit.next);
                return;
            }
            digit.value--;
        }
    }

    public class BigIntWrongInputException : System.Exception
    {
    }
}
