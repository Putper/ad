using System.Collections.Generic;

namespace AD
{
    public partial class MyQueue<T> : IMyQueue<T>
    {
        MyLinkedList<T> list = new MyLinkedList<T>();

        public bool IsEmpty()
        {
            return list.Size() == 0;
        }

        public void Enqueue(T data)
        {
            list.AddLast(data);
        }

        public T GetFront()
        {
            if (IsEmpty())
                throw new MyQueueEmptyException();
            return list.GetFirst();
        }

        public T Dequeue()
        {
            if (IsEmpty())
                throw new MyQueueEmptyException();
            T data = GetFront();
            list.RemoveFirst();
            return data;
        }

        public void Clear()
        {
            list.Clear();
        }

    }
}