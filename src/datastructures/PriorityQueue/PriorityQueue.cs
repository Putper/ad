﻿using System;
using System.Text;

namespace AD
{
    public partial class PriorityQueue<T> : IPriorityQueue<T>
        where T : IComparable<T>
    {
        public static int DEFAULT_CAPACITY = 100;
        public int size;   // Number of elements in heap
        public T[] array;  // The heap array

        //----------------------------------------------------------------------
        // Constructor
        //----------------------------------------------------------------------
        public PriorityQueue()
        {
            Clear();
        }

        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------
        public int Size()
        {
            return size;
        }

        public void Clear()
        {
            array = new T[DEFAULT_CAPACITY];
            size = 0;
        }

        public void Add(T x)
        {
            AddFreely(x);
            PercolateUp(size);
        }

        /// <summary>
        /// After an item has been added at the bottom,
        /// it should percolate up.
        /// Basically keep switching with parent to keep to the rule:
        /// Parents are always bigger than their children.
        /// </summary>
        /// <param name="index"></param>
        protected void PercolateUp(int index)
        {
            int parent = ParentIndex(index);
            T parentValue = array[parent];
            // 0 is always unused
            if (parent != 0 && parentValue.CompareTo(array[index]) > 0)
            {
                array[parent] = array[index];
                array[index] = parentValue;
                PercolateUp(parent);
            }
        }

        // Removes the smallest item in the priority queue
        public T Remove()
        {
            if (Size() == 0)
                throw new PriorityQueueEmptyException();

            // move the last element to the min position
            // and percolate it down
            T removed = array[1];
            array[1] = array[size];
            PercolateDown(1);

            size--;
            // downsize if possible
            if (size > DEFAULT_CAPACITY && size < array.Length / 3)
                Array.Resize(ref array, array.Length / 2);

            return removed;
        }

        /// <summary>
        /// Removing the smallest item creates a gap. This takes a node
        /// and keeps switching it with it's smallest child
        /// </summary>
        /// <param name="index"></param>
        protected void PercolateDown(int index)
        {
            int leftIndex = LeftChildIndex(index);
            int rightIndex = RightChildIndex(index);
            // if has left child and its smaller, switch and continue percolate
            if (
                leftIndex <= size // has left child
                && array[leftIndex].CompareTo(array[index]) < 0 // left is smaller than current
                && (rightIndex > size || array[leftIndex].CompareTo(array[rightIndex]) < 0)) // left is smaller than right (if it exists)
            {
                T leftValue = array[leftIndex];
                array[leftIndex] = array[index];
                array[index] = leftValue;
                PercolateDown(leftIndex);
                return;
            }
            // if right exists, switch with right and continue percolate
            if (rightIndex <= size && array[rightIndex].CompareTo(array[index]) < 0)
            {
                T rightValue = array[rightIndex];
                array[rightIndex] = array[index];
                array[index] = rightValue;
                PercolateDown(rightIndex);
            }
        }


        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for homework
        //----------------------------------------------------------------------

        public void AddFreely(T x)
        {
            // resize if needed
            if (size + 1 > array.Length / 2)
                Array.Resize(ref array, array.Length * 2);
            // ++size so it starts at index 1
            array[++size] = x;
        }

        public void BuildHeap()
        {
            for (int i = size / 2; i > 0; --i)
                PercolateDown(i);
        }


        //----------------------------------------------------------------------
        // non-API methods
        //----------------------------------------------------------------------
        protected int LeftChildIndex(int index)
        {
            return index * 2;
        }
        protected int RightChildIndex(int index)
        {
            return index * 2 + 1;
        }
        protected int ParentIndex(int index)
        {
            return index / 2;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for(int i=1; i<=size; ++i)
            {
                if (i != 1)
                    sb.Append(" ");
                sb.Append(array[i]);
            }
            return sb.ToString();
        }
    }
}
