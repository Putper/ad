﻿using System.Text;

namespace AD
{
    public partial class MyArrayList : IMyArrayList
    {
        private int[] data;
        private int size;

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="capacity"></param>
        public MyArrayList(int capacity)
        {
            data = new int[capacity];
            size = 0;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="n"></param>
        /// <exception cref="MyArrayListFullException"></exception>
        public void Add(int n)
        {
            if (Capacity() == size)
                throw new MyArrayListFullException();
            data[size] = n;
            size++;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        /// <exception cref="MyArrayListIndexOutOfRangeException"></exception>
        public int Get(int index)
        {
            if (index < 0 || index >= size)
                throw new MyArrayListIndexOutOfRangeException();
            return data[index];
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="index"></param>
        /// <param name="n"></param>
        /// <exception cref="MyArrayListIndexOutOfRangeException"></exception>
        public void Set(int index, int n)
        {
            // if out of range
            if (index < 0 || index >= size)
                throw new MyArrayListIndexOutOfRangeException();
            data[index] = n;
        }

        /// <summary>
        /// O(1)`
        /// </summary>
        /// <returns></returns>
        public int Capacity()
        {
            return data.Length;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        public int Size()
        {
            return size;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        public void Clear()
        {
            size = 0;
            data = new int[Capacity()];
        }

        /// <summary>
        /// O(n)
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public int CountOccurences(int n)
        {
            int count = 0;
            for(int i=0; i<size; ++i)
            {
                if (data[i] == n)
                    count++;
            }
            return count;
        }

        /// <summary>
        /// O(n)
        /// </summary>
        /// <returns>
        /// empty list: "NIL"
        /// if list has 2: "[2]"
        ///if list has 2,3,4: "[2,3,4]"
        /// </returns>
        public override string ToString()
        {
            if (size == 0)
                return "NIL";
            
            StringBuilder sb = new StringBuilder("[");
            for(int i=0; i<size; ++i)
            {
                // add comma except for first number
                if (i != 0)
                    sb.Append(",");
                sb.Append(data[i]);
            }
            sb.Append("]");

            return sb.ToString();
        }
    }
}
