namespace AD
{
    public partial class BinaryNode<T> : IBinaryNode<T>
    {
        public T data;
        public BinaryNode<T> left;
        public BinaryNode<T> right;

        public BinaryNode() : this(default(T)) { }
        public BinaryNode(T data) : this(data, default(BinaryNode<T>), default(BinaryNode<T>)) { }

        public BinaryNode(T data, BinaryNode<T> left, BinaryNode<T> right)
        {
            this.data = data;
            this.left = left;
            this.right = right;
        }

        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------
        public T GetData()
        {
            return data;
        }

        public BinaryNode<T> GetLeft()
        {
            return left;
        }

        public BinaryNode<T> GetRight()
        {
            return right;
        }

        public int Size()
        {
            int result = 1;
            if (left != null) result += left.Size();
            if (right != null) result += right.Size();
            return result;
        }

        public int NumberOfLeaves()
        {
            // I am a leaf!
            if (left == null && right == null)
                return 1;

            int result = 0;
            if (left != null) result += left.NumberOfLeaves();
            if (right != null) result += right.NumberOfLeaves();
            return result;
        }

        public int NumberOfNodesWithOneChild()
        {
            // Am I a node with one child?
            int result = ((left == null && right != null) || (right == null && left != null)) ? 1 : 0;
            // also check my brothers
            if (left != null) result += left.NumberOfNodesWithOneChild();
            if (right != null) result += right.NumberOfNodesWithOneChild();
            return result;
        }

        public int NumberOfNodesWithTwoChildren()
        {
            // Am I a node with two children?
            int result = (left != null && right != null) ? 1 : 0;
            // also check my brothers
            if (left != null) result += left.NumberOfNodesWithTwoChildren();
            if (right != null) result += right.NumberOfNodesWithTwoChildren();
            return result;
        }

        public string ToPrefixString()
        {
            string leftString = (left != null)
                ? left.ToPrefixString()
                : "NIL";

            string rightString = (right != null)
                ? right.ToPrefixString()
                : "NIL";

            return $"[ {data} {leftString} {rightString} ]";
        }

        public string ToInfixString()
        {
            string leftString = (left != null)
                ? left.ToInfixString()
                : "NIL";

            string rightString = (right != null)
                ? right.ToInfixString()
                : "NIL";

            return $"[ {leftString} {data} {rightString} ]";
        }

        public string ToPostfixString()
        {
            string leftString = (left != null)
                ? left.ToPostfixString()
                : "NIL";

            string rightString = (right != null)
                ? right.ToPostfixString()
                : "NIL";

            return $"[ {leftString} {rightString} {data} ]";
        }
    }
}