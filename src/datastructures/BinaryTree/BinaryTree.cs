using System;
using System.Xml.Linq;

namespace AD
{
    public partial class BinaryTree<T> : IBinaryTree<T>
    {
        public BinaryNode<T> root;

        //----------------------------------------------------------------------
        // Constructors
        //----------------------------------------------------------------------

        public BinaryTree()
        {
            root = null;
        }

        public BinaryTree(T rootItem)
        {
            root = new BinaryNode<T>(rootItem, null, null);
        }


        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------

        public BinaryNode<T> GetRoot()
        {
            return root;
        }

        public int Size()
        {
            if (root == null)
                return 0;
            return root.Size();
        }

        public int Height()
        {
            return IsEmpty()
                ? -1
                : Height(root) - 1;
        }

        private int Height(BinaryNode<T> node)
        {
            if (node == null)
                return 0;
            return Math.Max(
                Height(node.left),
                Height(node.right)
            ) + 1;
        }

        public void MakeEmpty()
        {
            root = null;
        }

        public bool IsEmpty()
        {
            return root == null;
        }

        public void Merge(T rootItem, BinaryTree<T> t1, BinaryTree<T> t2)
        {
            // avoid merging same trees
            if (t1.root == t2.root && t1.root != null)
                throw new System.ArgumentException();

            // create new root with t1 on left and t2 on right
            root = new BinaryNode<T>(rootItem, t1.root, t2.root);

            // empty other trees, now that they're all here :)
            if (this != t1)
                t1.root = null;
            if (this != t2)
                t2.root = null;
        }

        public string ToPrefixString()
        {
            if (root == null)
                return "NIL";
            return root.ToPrefixString();
        }

        public string ToInfixString()
        {
            if (root == null)
                return "NIL";
            return root.ToInfixString();
        }

        public string ToPostfixString()
        {
            if (root == null)
                return "NIL";
            return root.ToPostfixString();
        }


        //----------------------------------------------------------------------
        // Interface methods : methods that have to be implemented for homework
        //----------------------------------------------------------------------

        public int NumberOfLeaves()
        {
            if (root == null)
                return 0;
            return root.NumberOfLeaves();
        }

        public int NumberOfNodesWithOneChild()
        {
            if (root == null)
                return 0;
            return root.NumberOfNodesWithOneChild();
        }

        public int NumberOfNodesWithTwoChildren()
        {
            if (root == null)
                return 0;
            return root.NumberOfNodesWithTwoChildren();
        }
    }
}