﻿using System.Data.Common;
using System.Text;

namespace AD
{
    public partial class MyLinkedList<T> : IMyLinkedList<T>
    {
        public MyLinkedListNode<T> first;
        public MyLinkedListNode<T> last;
        private int size;

        public MyLinkedList()
        {
            Clear();
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="data"></param>
        public void AddFirst(T data)
        {
            MyLinkedListNode<T> node = new MyLinkedListNode<T>(data, first);
            first = node;
            // if its first node being added, its also automatically last node in the list
            if (size == 0)
                last = node;
            size++;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="data"></param>
        public void AddLast(T data)
        {
            // if list is empty, its the same as adding first
            if (size == 0)
            {
                AddFirst(data);
                return;
            }
            MyLinkedListNode<T> node = new MyLinkedListNode<T>(data);
            last.next = node;
            last = node;
            size++;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        /// <exception cref="MyLinkedListEmptyException"></exception>
        public T GetFirst()
        {
            if (Size() == 0)
                throw new MyLinkedListEmptyException();
            return first.data;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        /// <exception cref="MyLinkedListEmptyException"></exception>
        public T GetLast()
        {
            if (Size() == 0)
                throw new MyLinkedListEmptyException();
            return last.data;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        public void RemoveFirst()
        {
            if (Size() == 0)
                throw new MyLinkedListEmptyException();
            first = first.next;
            size--;
            // if list is now empty update the last variable as well
            if (size == 0)
                last = null;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        public int Size()
        {
            return size;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        public void Clear()
        {
            first = null;
            last = null;
            size = 0;
        }

        /// <summary>
        /// O(n)
        /// </summary>
        /// <param name="index"></param>
        /// <param name="data"></param>
        /// <exception cref="MyLinkedListIndexOutOfRangeException"></exception>
        public void Insert(int index, T data)
        {
            // has to be within size of list
            if (index < 0 || index > size)
                throw new MyLinkedListIndexOutOfRangeException();
            // if first use existing method
            if (index == 0)
            {
                AddFirst(data);
                return;
            }

            // go to index where to insert
            MyLinkedListNode<T> current = first;
            for (int i=0; i<index-1; ++i)
            {
                current = current.next;
            }
            // insert it right there
            MyLinkedListNode<T> newNode = new MyLinkedListNode<T>(data, current.next);
            current.next = newNode;
            size++;

            // if its the last in the list, mark it as such
            if (newNode.next == null)
                last = newNode;
        }

        /// <summary>
        /// O(n)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (size == 0)
                return "NIL";

            StringBuilder sb = new StringBuilder("[");
            MyLinkedListNode<T> currentNode = first;
            while (currentNode != null)
            {
                if (!currentNode.Equals(first))
                    sb.Append(",");
                sb.Append(currentNode.data);
                currentNode = currentNode.next;
            }
            sb.Append("]");

            return sb.ToString();
        }
    }
}