using System.Text;

namespace AD
{
    public partial class FirstChildNextSiblingNode<T> : IFirstChildNextSiblingNode<T>
    {
        public FirstChildNextSiblingNode<T> firstChild;
        public FirstChildNextSiblingNode<T> nextSibling;
        public T data;

        public FirstChildNextSiblingNode(T data, FirstChildNextSiblingNode<T> firstChild, FirstChildNextSiblingNode<T> nextSibling)
        {
            this.data = data;
            this.firstChild = firstChild;
            this.nextSibling = nextSibling;
        }

        public FirstChildNextSiblingNode(T data)
        {
            this.data = data;
            firstChild = null;
            nextSibling = null;
        }

        public T GetData()
        {
            return data;
        }

        public FirstChildNextSiblingNode<T> GetFirstChild()
        {
            return firstChild;
        }

        public FirstChildNextSiblingNode<T> GetNextSibling()
        {
            return nextSibling;
        }

        public int Size()
        {
            int result = 1;
            if (firstChild != null) result += firstChild.Size();
            if (nextSibling != null) result += nextSibling.Size();
            return result;
        }

        public override string ToString()
        {
            StringBuilder toReturn = new StringBuilder();
            toReturn.Append(data.ToString());
            if (firstChild != null)
            {
                toReturn.Append(",FC(");
                toReturn.Append(firstChild.ToString());
                toReturn.Append(")");
            }
            if (nextSibling != null)
            {
                toReturn.Append(",NS(");
                toReturn.Append(nextSibling.ToString());
                toReturn.Append(")");
            }

            return toReturn.ToString();
        }
    }
}