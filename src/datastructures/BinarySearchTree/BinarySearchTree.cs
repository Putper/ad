using System.Text;

namespace AD
{
    public partial class BinarySearchTree<T> : BinaryTree<T>, IBinarySearchTree<T>
        where T : System.IComparable<T>
    {

        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------

        public void Insert(T x)
        {
            var toInsert = new BinaryNode<T>(x);

            if (IsEmpty())
            {
                root = toInsert;
                return;
            }
            Insert(root, toInsert);
        }

        protected void Insert(BinaryNode<T> current, BinaryNode<T> toInsert)
        {
            if (current.data.Equals(toInsert.data))
                throw new BinarySearchTreeDoubleKeyException();

            // insert right if bigger
            if (toInsert.data.CompareTo(current.data) > 0)
            {
                if (current.right == null)
                    current.right = toInsert;
                else
                    Insert(current.right, toInsert);
            }
            // insert left if smaller
            else
            {
                if (current.left == null)
                    current.left = toInsert;
                else
                    Insert(current.left, toInsert);
            }
        }

        public T FindMin()
        {
            if (IsEmpty())
                throw new BinarySearchTreeEmptyException();

            return FindMin(root).data;
        }

        protected BinaryNode<T> FindMin(BinaryNode<T> node)
        {
            return (node.left != null) ? FindMin(node.left) : node;
        }

        public void RemoveMin()
        {
            if (root == null)
                throw new BinarySearchTreeEmptyException();
            root = RemoveMin(root);
        }

        protected BinaryNode<T> RemoveMin(BinaryNode<T> node)
        {
            if (node.left == null)
                return node.right;

            node.left = RemoveMin(node.left);
            return node;
        }

        public void Remove(T x)
        {
            Remove(root, x);
        }

        public BinaryNode<T> Remove(BinaryNode<T> current, T toRemove)
        {
            if (current == null)
                throw new BinarySearchTreeElementNotFoundException();

            int comparison = toRemove.CompareTo(current.data);
            //find node to remove
            if (comparison > 0)
                current.right = Remove(current.right, toRemove);
            else if (comparison < 0)
                current.left = Remove(current.left, toRemove);
            // if found node to remove, and has left & right children
            // move the min of the right neighbour to here
            else if (current.left != null && current.right != null)
            {
                current.data = FindMin(current.right).data;
                current.right = RemoveMin(current.right);
            }
            // if found node to remove and only has 1 child,
            // just replace the node that needs to be removed with the child
            else
                current = current.left ?? current.right;

            return current;
        }

        public string InOrder()
        {
            return InOrder(root);
        }

        protected string InOrder(BinaryNode<T> node)
        {
            if (node == null)
                return string.Empty;
            string left = InOrder(node.left);
            string right = InOrder(node.right);
            return $"{left} {node.data} {right}".Trim();
        }

        public override string ToString()
        {
            return InOrder();
        }
    }
}
