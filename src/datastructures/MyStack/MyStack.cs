namespace AD
{
    public partial class MyStack<T> : IMyStack<T>
    {
        protected IMyLinkedList<T> list = new MyLinkedList<T>();

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            return list.Size() == 0;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        /// <exception cref="MyStackEmptyException"></exception>
        public T Pop()
        {
            var first = Top();
            if (first == null)
                throw new MyStackEmptyException();

            list.RemoveFirst();
            return first;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="data"></param>
        public void Push(T data)
        {
            list.AddFirst(data);
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        /// <exception cref="MyStackEmptyException"></exception>
        public T Top()
        {
            try
            {
                return list.GetFirst();
            }
            catch (MyLinkedListEmptyException)
            {
                throw new MyStackEmptyException();
            }
        }
    }
}
