using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AD
{
    public partial class Graph : IGraph
    {
        public static readonly double INFINITY = System.Double.MaxValue;

        public Dictionary<string, Vertex> vertexMap;


        //----------------------------------------------------------------------
        // Constructor
        //----------------------------------------------------------------------

        public Graph()
        {
            vertexMap = new Dictionary<string, Vertex>();
        }


        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------

        /// <summary>
        ///    Adds a vertex to the graph. If a vertex with the given name
        ///    already exists, no action is performed.
        /// </summary>
        /// <param name="name">The name of the new vertex</param>
        public void AddVertex(string name)
        {
            // add if not already known
            if (!vertexMap.ContainsKey(name))
                vertexMap.Add(name, new Vertex(name));
        }


        /// <summary>
        ///    Gets a vertex from the graph by name. If no such vertex exists,
        ///    a new vertex will be created and returned.
        /// </summary>
        /// <param name="name">The name of the vertex</param>
        /// <returns>The vertex with the given name</returns>
        public Vertex GetVertex(string name)
        {
            vertexMap.TryGetValue(name, out Vertex vertex);
            if(vertex == null)
            {
                vertex = new Vertex(name);
                vertexMap.Add(name, vertex);
            }
            return vertex;
        }


        /// <summary>
        ///    Creates an edge between two vertices. Vertices that are non existing
        ///    will be created before adding the edge.
        ///    There is no check on multiple edges!
        /// </summary>
        /// <param name="source">The name of the source vertex</param>
        /// <param name="dest">The name of the destination vertex</param>
        /// <param name="cost">cost of the edge</param>
        public void AddEdge(string source, string dest, double cost = 1)
        {
            // get vertexes
            Vertex sourceVertex = GetVertex(source);
            Vertex destVertex = GetVertex(dest);
            // add edge targeting dest to the source's list of adjacents
            Edge edge = new Edge(destVertex, cost);
            sourceVertex.GetAdjacents().AddLast(edge);
        }


        /// <summary>
        ///    Clears all info within the vertices. This method will not remove any
        ///    vertices or edges.
        /// </summary>
        public void ClearAll()
        {
            foreach (KeyValuePair<string, Vertex> kv in vertexMap)
                foreach (Edge edge in kv.Value.GetAdjacents())
                    edge.dest.Reset();
        }

        /// <summary>
        ///    Performs the Breatch-First algorithm for unweighted graphs.
        /// </summary>
        /// <param name="name">The name of the starting/root vertex</param>
        public void Unweighted(string name)
        {
            // root has a distance of 0, its the start
            Vertex root = GetVertex(name);
            root.distance = 0;

            Queue<Vertex> queue = new Queue<Vertex>();
            queue.Enqueue(root);

            // explore first in queue, and add all adjacents
            // to the queue to explore them as well
            while (queue.Count > 0)
            {
                Vertex vertex = queue.Dequeue();
                foreach(Edge edge in vertex.GetAdjacents())
                {
                    Vertex neighbour = edge.dest;
                    if (neighbour.distance == INFINITY)
                    {
                        neighbour.distance = vertex.distance + 1;
                        neighbour.prev = vertex;
                        queue.Enqueue(neighbour);
                    }
                }
            }

        }

        /// <summary>
        ///    Performs the Dijkstra algorithm for weighted graphs.
        /// </summary>
        /// <param name="name">The name of the starting vertex</param>
        public void Dijkstra(string name)
        {
            ClearAll();

            Vertex root = GetVertex(name);
            root.distance = 0;

            PriorityQueue<Vertex> queue = new PriorityQueue<Vertex>();
            queue.Add(root);

            while (queue.Size() > 0)
            {
                Vertex vertex = queue.Remove();
                if (vertex.known)
                    continue;
                vertex.known = true;

                foreach (Edge edge in vertex.GetAdjacents())
                {
                    Vertex neighbour = edge.dest;
                    if (neighbour.known)
                        continue;
                    // the distance to the adjacent is the distance so far PLUS the cost of the edge
                    double distance = vertex.distance + edge.cost;
                    //// if its shorter, mark it
                    if (distance < neighbour.distance)
                    {
                        neighbour.distance = distance;
                        neighbour.prev = vertex;
                    }
                    queue.Add(neighbour);
                }
            }
        }

        //----------------------------------------------------------------------
        // ToString that has to be implemented for exam
        //----------------------------------------------------------------------

        /// <summary>
        ///    Converts this instance of Graph to its string representation.
        ///    It will call the ToString method of each Vertex. The output is
        ///    ascending on vertex name.
        /// </summary>
        /// <returns>The string representation of this Graph instance</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in vertexMap.Keys.OrderBy(x => x))
            {
                Vertex vertex = vertexMap[key];
                sb.Append(vertex.ToString());
            }
            return sb.ToString();
        }


        //----------------------------------------------------------------------
        // Interface methods : methods that have to be implemented for homework
        //----------------------------------------------------------------------



        public bool IsConnected()
        {
            throw new System.NotImplementedException();
        }

    }
}