using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace AD
{
    public partial class Vertex : IVertex, IComparable<Vertex>
    {
        public string name;
        public LinkedList<Edge> adj;
        public double distance;
        public Vertex prev;
        public bool known; // aka explored


        //----------------------------------------------------------------------
        // Constructor
        //----------------------------------------------------------------------

        /// <summary>
        ///    Creates a new Vertex instance.
        /// </summary>
        /// <param name="name">The name of the new vertex</param>
        public Vertex(string name)
        {
            this.name = name;
            adj = new LinkedList<Edge>();
            Reset();
        }


        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------

        public string GetName()
        {
            return name;
        }
        public LinkedList<Edge> GetAdjacents()
        {
            return adj;
        }

        public double GetDistance()
        {
            return distance;
        }

        public Vertex GetPrevious()
        {
            return prev;
        }

        public bool GetKnown()
        {
            return known;
        }

        /// <summary>
        /// Resets prev, distance (infinity) and known for a vertex
        /// </summary>
        public void Reset()
        {
            prev = null;
            distance = Graph.INFINITY;
            known = false;
        }


        //----------------------------------------------------------------------
        // ToString that has to be implemented for exam
        //----------------------------------------------------------------------

        /// <summary>
        ///    Converts this instance of Vertex to its string representation.
        ///    <para>Output will be like : name (distance) [ adj1 (cost) adj2 (cost) .. ]</para>
        ///    <para>Adjecents are ordered ascending by name. If no distance is
        ///    calculated yet, the distance and the parantheses are omitted.</para>
        /// </summary>
        /// <returns>The string representation of this Graph instance</returns> 
        public override string ToString()
        {
            // starts with name V1
            StringBuilder sb = new StringBuilder(name);
            // if have distance, add it between (): V1(5)
            if (distance < Graph.INFINITY)
                sb.Append($"({distance})");
            // add opening bracket for adjacents: V1(5)[
            sb.Append("[");
            // add adjacents with their cost: V1(5)[V3(3) V4(10)
            foreach (Edge edge in adj.OrderBy(x => x.dest.name))
            {
                sb.Append($" {edge.dest.name}({edge.cost})");
            }
            // close bracket V1(5)[ V3(3) V4(10) ]
            sb.Append(" ]");
            return sb.ToString();
        }

        public int CompareTo([AllowNull] Vertex other)
        {
            return distance.CompareTo(other.distance);
        }
    }
}